const mongoose = require('mongoose');
const app = require('./api/app');
const { PORT } = require('./utils/constants');
const { MONGODB_CONNECTION_STRING } = require('./utils/strings');

// connect to mongo database
mongoose.connect(MONGODB_CONNECTION_STRING, {
    useCreateIndex: true,
    useNewUrlParser: true 
}).then(() => {
    console.log('[database] mlab mongo database connection success!');
}).catch(err => console.error(err));

const server = app.listen(PORT, () => {
    console.log('server running at http://localhost:' + server.address().port);
});
