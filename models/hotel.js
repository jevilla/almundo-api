const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const hotelSchema = new Schema({
    id: String,
    name: String,
    stars: Number,
    price: Number,
    image: String,
    amenities: [String]
}, { collection: 'hotel' });

const hotel = mongoose.model('hotel', hotelSchema);
module.exports = hotel;
