const express = require('express');
const router = require('../routes/hotel');
const bodyParser = require('body-parser');
const handlers = require('../../utils/helpers/handlers');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('assets'));
app.use(handlers.accessControl);

app.use('/api', router);
app.use(handlers.requestHandler);
app.use(handlers.errorHandler);

module.exports = app;
