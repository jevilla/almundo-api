const express = require('express');
const hotel = require('../../models/hotel');
const router = express.Router();

router.get('/hotels', (req, res, next) => {
    hotel.find({}).then(hotels => {
        res.send(hotels)
    }).catch(err => {next(err)});
});

router.post('/hotels/stars/', (req, res, next) => {
    const {stars} = req.body;
    hotel.find({
        $or: stars
    }).then(doc => {
        res.send(doc);
    }).catch(err => next(err));
});

router.get('/hotel/:name', (req, res, next) => {
    hotel.find({ name: new RegExp(`^${req.params.name}$`, 'i') }).then(doc => {
        res.send(doc);
    }).catch(err => next(err));
});

module.exports = router;
