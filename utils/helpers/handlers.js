const { ROUTE_NOT_FOUND } = require('../strings');

exports.requestHandler = (req, res, next) => {
    const err = new Error(ROUTE_NOT_FOUND);
    err.status = 404;
    next(err);
};

exports.errorHandler = (err, req, res, next) => {
    res.status(err.status || 500);
    res.json({
        error: { message: err.message }
    });
};

exports.accessControl = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
};