const constants = require('./constants');

exports.MONGODB_CONNECTION_STRING = `mongodb://${constants.DB_USER}:${constants.DB_PASSWORD}@ds137596.mlab.com:37596/almundo`;
exports.ROUTE_NOT_FOUND = 'The requested URL was not found on this server.';