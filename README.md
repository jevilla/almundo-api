This project was created using Node.js and Express.js, and using nosql database (mongo this case).
[mLab](https://mlab.com) was used to store the database.

To be able to run this project it is required have installed node and npm.

After download or clone the repo you should run the following scripts in order to run the app.
## Intall the node_modules:
`npm install`.

## Run the app

- Open a simulator from Android studio

- In the project directory, you can run `node start`

## The Restful API was hosted in Heroku, these are the endpoints:

- To get all hotels: https://almundo-api-test.herokuapp.com/api/hotels

- To get a specific hotel: https://almundo-api-test.herokuapp.com/api/hotel/[name of the hotel]

- To get an image: https://almundo-api-test.herokuapp.com/images/hotels/[name of the image]
for example: https://almundo-api-test.herokuapp.com/images/hotels/917bd6d1_b.jpg
